import pygame
import sys
import random

class DVDLogo:
    def __init__(self, image_path, screen_width, screen_height):
        # DVD logo image
        self.image = pygame.image.load(image_path)
        self.image = pygame.transform.scale(self.image, (100, 50))
        # Initial position
        self.x = 0
        self.y = 0
        # Initial velocity
        self.dx = 5
        self.dy = 5
        # Affect screen characteristics
        self.screen_width = screen_width
        self.screen_height = screen_height
        # Set attributes
        self.color = self.random_color()
        self.color_change = True
        self.trace = False

    # Function to generate a random RGB color
    def random_color(self):
        return (random.randint(10, 255), random.randint(10, 255), random.randint(10, 255))

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_c:
                    # Active the logo color change option when the C key is pressed
                    self.color_change = not self.color_change
                elif event.key == pygame.K_t:
                    # Active the logo trace option when the T key is pressed
                    self.trace = not self.trace

    def move(self):
        # Move the logo
        self.x += self.dx
        self.y += self.dy

        # Bounce off the left and right edges
        if self.x <= 0 or self.x + self.image.get_width() >= self.screen_width:
            self.dx = -self.dx

        # Bounce off the top and bottom edges
        if self.y <= 0 or self.y + self.image.get_height() >= self.screen_height:
            self.dy = -self.dy

        # If the logo color change option was activated and dvd logo bounce off one edge, it changes of color.
        if self.color_change and (self.x == 0 or self.x + self.image.get_width() == self.screen_width or self.y == 0 or self.y + self.image.get_height() == self.screen_height):
            self.color = self.random_color()

    # Draw the logo with the current color
    def draw(self, screen):
        if not self.trace:
            # Set background color
            screen.fill((0, 0, 0))

        dvd_logo_with_color = self.image.copy()
        dvd_logo_with_color.fill(self.color, special_flags=pygame.BLEND_ADD)
        screen.blit(dvd_logo_with_color, (self.x, self.y))
        pygame.display.update()

def main():
    # Initialize Pygame
    pygame.init()
    # Screen dimensions
    screen_width, screen_height = 800, 600 # like 4:3 monitor
    screen = pygame.display.set_mode((screen_width, screen_height))
    pygame.display.set_caption("DVD Screen Saver")

    # DVD logo
    dvd = DVDLogo("img/dvd_logo.png", screen_width, screen_height)

    clock = pygame.time.Clock()

    # Set background color
    screen.fill((0, 0, 0))

    while True:
        dvd.handle_events()
        dvd.move()
        dvd.draw(screen)
        clock.tick(60)

if __name__ == '__main__':
    main()
