# DVD Screensaver

## Context

The DVD screensaver is a popular screensaver that gained popularity in the late 1990s and early 2000s. It was a built-in screensaver on many DVD players and was also commonly used on personal computers and television screens.

| Official DVD logo |
|:---:|
| ![dvd logo](./img/dvd_logo.png) |

The DVD screensaver typically featured a simple company logo or text (often the word "DVD") that would bounce around the screen. The logo would move in a random path, with a random color, bouncing off the edges of the screen like a bouncing ball.

Over time, the DVD screensaver became a cultural icon, symbolizing the early days of DVD technology and the nostalgia associated with it. It has been featured in various pop culture references, including movies, TV shows, and memes.

## Tasks

- [x]   to display the DVD logo.
- [x]   to move the DVD logo.
- [x]   to manage bouncing off the edges.
- [x]   to add the DVD logo color change option.
- [x]   to add the DVD logo trace option.

